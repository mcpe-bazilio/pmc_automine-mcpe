<?php
namespace PMC_AutoMine;

use PMC_AutoMine\Tasks\MineResetTask;
use pocketmine\level\format\Chunk;
use pocketmine\level\format\io\region\Anvil;
use pocketmine\level\format\io\region\McRegion;
use pocketmine\level\format\io\region\PMAnvil;
use pocketmine\level\Level;
use pocketmine\math\Vector3;
use pocketmine\level\Position;

class Mine {

	/** @var Vector3 $a минимальная вегшина шахты */
	public $pos1;

	/** @var Vector3 $a максимальная вегшина шахты */
	public $pos2;

	/** @var Vector3 $a место входа в шахту */
	public $spawnPos;

	/** @var array $rules правило заполнения шахты */
	public $rules;

	public $autoReset = false;

	/** @var array $resetArray массив идентификаторов блоков и мин процентов обновления. */
	public $resetArray = [];

	/** @var int $fillBase суммарное количество частей заполнения всех элеменотов заполнения */
	public $fillBase = 0;

	/** @var Level $level мир шахты */
	public $level;

	/** @var PMC_AutoMine $plugin */
	private $plugin;

	public $mineName;

	/** @var array $tags произвольные данные шахты */
	public $tags;

	public function __construct(PMC_AutoMine $plugin, $mineName, Vector3 $pos1, Vector3 $pos2, Level $level, array $rules = [], Vector3 $spawnPos = null, $tags = []){
		$this->pos1 = new Vector3(min($pos1->getX(), $pos2->getX()), min($pos1->getY(), $pos2->getY()), min($pos1->getZ(), $pos2->getZ()));
		$this->pos2 = new Vector3(max($pos1->getX(), $pos2->getX()), max($pos1->getY(), $pos2->getY()), max($pos1->getZ(), $pos2->getZ()));
		$this->spawnPos = $spawnPos == null ? $this->pos2 : $spawnPos;
		$this->plugin = $plugin;
		$this->level = $level;
		$this->mineName = $mineName;
		$this->tags = $tags;
		$this->setRules($rules);
	}

	public function isRulesSet(){
		return (count($this->rules) > 0);
	}

	private function reculcMineParams(){
		$this->fillBase = 0;
		$this->autoReset = false;
		$this->resetArray = [];
		foreach($this->rules as $itemId => $rule){
			if(isset($rule['min-fill-ratio'])){
				$this->autoReset = true;
				$this->resetArray[$itemId] = $rule['min-fill-ratio']; //TODO тут отрефакторить после проверки, как работает дамадж
			}
			$this->fillBase += $rule['fill-part'];
		}
	}

	public function getPos1(){
		return $this->pos1;
	}

	public function getPos2(){
		return $this->pos2;
	}

	public function movePos($posNum, $axe, $delta){
		$p = intval($posNum) == 1 ? 'pos1' : 'pos2';
		switch(strtolower($axe)){
			case "x":
				$this->$p = $this->$p->add($delta, 0, 0);
				break;
			case "y":
				$y = $this->$p->getY() + $delta;
				$delta = max(1, min($this->level->getProvider()->getWorldHeight() - 2, $y)) - $this->$p->getY();
				$this->$p = $this->$p->add(0, $delta, 0);
				break;
			case "z":
				$this->$p = $this->$p->add(0, 0, $delta);
				break;
			default:
		}
		$this->updatePluginMines();
	}

	public function getName(){
		return $this->mineName;
	}

	public function getSpawnPos(){
		return $this->spawnPos;
	}

	public function getLevel(){
		return $this->level;
	}

	public function getRules(){
		return $this->rules;
	}

	private function setRules(array $rules){
		$this->rules = $rules;
		$this->reculcMineParams();
	}

	//TODO проверить
	public function addRule($itemId, $fillPart, $minpc = null){
		if(!preg_match('/(\d+)(?::(\d+))*/m', $itemId, $matches)) return false;
		$itemId = (intval($matches[1]) & PMC_AutoMine::ITEM_ID_RESTRICT);
		if(isset($matches[2]) && intval($matches[2]) > 0){
			$itemId .= ':' . (intval($matches[2]) & PMC_AutoMine::ITEM_DAMAGE_RESTRICT);
		}
		if(isset($this->rules[$itemId])) $this->rules[$itemId] = [];
		$rule = &$this->rules[$itemId];
		if($minpc != null){
			if(intval($minpc) > 0){
				$rule['min-fill-ratio'] = intval($minpc);
			}else{
				unset($rule['min-fill-ratio']);
			}
		}
		$rule['fill-part'] = $fillPart;
		$rule['name'] = $this->plugin->getItemNameRU($itemId);
		$this->reculcMineParams();
		$this->updatePluginMines();
		return true;
	}

	//TODO проверить
	public function deleteRule($itemId){
		if(isset($this->rules[$itemId])){
			unset($this->rules[$itemId]);
			$this->reculcMineParams();
			$this->updatePluginMines();
		}
	}

	public function getRulesInfo($otstup = ''){
		$p = $this->plugin;
		$c = '§a';
		$v = '§d';
		$info = '';
		$idmePad = 0;
		$namePad = 0;
		$partPad = 0;
		foreach($this->rules as $itemId => $rule){
			$namePad = max(mb_strlen($rule['name']), $namePad);
			$idmePad = max(mb_strlen($itemId), $idmePad);
			$partPad = max(mb_strlen($rule['fill-part']), $partPad);
		}
		foreach($this->rules as $itemId => $rule){
			$info .= "\n" . $otstup . $c . "id $v" . str_pad($itemId, $idmePad) . " §7" . $p->mb_str_pad($rule['name'], $namePad, ' ', STR_PAD_RIGHT) . " {$c}- $v"
				. str_pad($rule['fill-part'], $partPad, ' ', STR_PAD_LEFT) . "/" . $this->fillBase . "{$c}ч §9($v"
				. floor($rule['fill-part'] / $this->fillBase * 100) . "%§9)";
			if(isset($rule['min-fill-ratio'])){
				$info .= ". §7Автообновлнение при $v" . $rule['min-fill-ratio'] . "%";
			}
		}
		return $info;
	}

	public function getInfo($bRules = true){
		$p = $this->plugin;
		$msg = "\n§eИмя: " . $p->fri($this->getName(), 3)
			. '§e, спаун: ' . $p->fCrd($this->getSpawnPos())
			. '§e, вершина 1: ' . $p->fCrd($this->getPos1())
			. '§e, вершина 2: ' . $p->fCrd($this->getPos2())
			. '§e, объем: ' . $p->ec($this->getVolume(), 3, '§d');

		if($bRules){
			$rulesInfo = $this->getRulesInfo('     ');
			$msg .= "§e, заполнение" . ($rulesInfo == '' ? " не задано." : ": " . $rulesInfo);
		}
		return $msg;
	}

	private function updatePluginMines(){
		$this->plugin->mines[strtolower($this->mineName)] = $this;
		$this->plugin->saveMinesConfig();
	}

	public function getVolume(){
		$p1 = $this->pos1;
		$p2 = $this->pos2;
		return (abs($p1->getFloorX() - $p2->getFloorX()) + 1) * (abs($p1->getFloorY() - $p2->getFloorY()) + 1) * (abs($p1->getFloorZ() - $p2->getFloorZ()) + 1);
	}

	public function fillMine($bClear = false){
		$chunks = [];
		$level = $this->level;
		/** @var Anvil|PMAnvil|McRegion $provider */
		$p1 = $this->pos1;
		$p2 = $this->pos2;
		$chunkClass = null;
		for($x = $p1->getX(); $x - 16 <= $p2->getX(); $x += 16){
			for($z = $p1->getZ(); $z - 16 <= $p2->getZ(); $z += 16){
				/** @var Chunk $chunk */
				$chunk = $level->getChunk($x >> 4, $z >> 4, true);
				$chunkClass = get_class($chunk);
				$chunks[Level::chunkHash($x >> 4, $z >> 4)] = $chunk->fastSerialize();
			}
		}
		if(count($chunks) < 1) return;
		//TODO сообщение
		$levelId = $level->getId();
		$p = $this->plugin;
		$blockZoneId = $p->getRegionBlocker()->blockZone($p1, $p2, $level);
		/** @noinspection PhpUndefinedVariableInspection */
		$rules = $bClear ? [0 => ['fill-part' => 1]] : $this->rules;
		$resetTask = new MineResetTask($chunks, $p1, $p2, $rules, $levelId, $blockZoneId, $chunkClass, $this->mineName);
		$p->scheduleReset($resetTask);
	}

	/**
	 * проверяет, не настала ли пора обновлять шахту.
	 * Проверяет, включено ли для шахты автообновление (хотя бы для одного ресурса)
	 * Если включено, то проверяет остатки.
	 *
	 * @param $info
	 *
	 * @return bool
	 */
	public function checkNeedReset(&$info){
		$info = '';
		if(!$this->autoReset) return false;
		$p1 = $this->getPos1();
		$p2 = $this->getPos2();
		$level = $this->level;
		//TODO дописать
		$composition = [];
		for($x = $p1->getX(); $x <= $p2->getX(); $x++){
			for($y = $p1->getY(); $y <= $p2->getY(); $y++){
				for($z = $p1->getZ(); $z <= $p2->getZ(); $z++){
					$block = $level->getBlock(new Vector3($x, $y, $z));
					$itemId = '' . $block->getId();
					$damage = $block->getDamage();
					if($damage > 0) $itemId .= ':' . $damage;
					if(isset($composition[$itemId])){
						$composition[$itemId]++;
					}else{
						$composition[$itemId] = 1;
					}
				}
			}
		}
		$volume = $this->getVolume();
		foreach($this->rules as $itemId => $rule){
			if(!isset($rule['min-fill-ratio'])) continue;
			if(!isset($composition[$itemId])){
				$info = "itemId $itemId (" . $rule['name'] . ") не найден в шахте (д.б. мин " . $rule['min-fill-ratio'] . "%)";
				return true;
			}
			if($rule['min-fill-ratio'] > ($composition[$itemId] / $volume) * 100){
				$info = "itemId $itemId (" . $rule['name'] . ") осталось менее " . $rule['min-fill-ratio'] . "%)";
				return true;
			}
		}
		return false;
	}

	public function insideMine($xv, $y = 0, $z = 0){
		if($xv instanceof Vector3){
			$x = $xv->getX();
			$y = $xv->getY();
			$z = $xv->getZ();
		}else{
			$x = $xv;
		}

		if($x < $this->pos1->getX() || $x > $this->pos2->getX()) return false;
		if($z < $this->pos1->getZ() || $z > $this->pos2->getZ()) return false;
		if($y < $this->pos1->getY() || $y > $this->pos2->getY()) return false;
		return true;
	}


	/**
	 * @param Vector3|Position $v1
	 * @param Vector3|Position $v2
	 *
	 * @return bool
	 */
	public function intersectWithMine($v1, $v2){
		$x0 = min($v1->getX(), $v2->getX());
		$x1 = max($v1->getX(), $v2->getX());
		if($x1 < $this->pos1->getX() || $x0 > $this->pos2->getX()) return false;

		$z0 = min($v1->getZ(), $v2->getZ());
		$z1 = max($v1->getZ(), $v2->getZ());
		if($z1 < $this->pos1->getZ() || $z0 > $this->pos2->getZ()) return false;

		$y0 = min($v1->getY(), $v2->getY());
		$y1 = max($v1->getY(), $v2->getY());
		if($y1 < $this->pos1->getY() || $y0 > $this->pos2->getY()) return false;

		return true;
	}


}
