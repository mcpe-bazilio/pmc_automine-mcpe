<?php

namespace PMC_AutoMine\Commands;

use PMC_AutoMine\Mine;
use PMC_AutoMine\PMC_AutoMine;
use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\level\Position;
use pocketmine\math\Vector3;
use pocketmine\Player;

class MineCommands implements CommandExecutor {

	/** @var PMC_AutoMine $plugin */
	public $plugin;

	public function __construct(PMC_AutoMine $Plugin){
		$this->plugin = $Plugin;
	}

	/**
	 * @param CommandSender $s
	 * @param Command       $cmd
	 * @param string        $label
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function onCommand(CommandSender $s, Command $cmd, $label, array $args){
		$p = $this->plugin;
		if($cmd->getName() != "mine") return false;
		if(!$s->hasPermission("mine")) return $p->MSG($s, 0, "[✘] У Вас нет прав для выполнения команды §e/" . $cmd->getName());
		$p->normalizeCommandArgs($args);//Убираем пустые ргументы
		if(!isset($args[0])) return $p->MSG($s, 1, $p->getCmdUsage("mine", 1, true));

		$subCmd = $subCmdOrig = strtolower($args[0]);
		//Если первый параметр - не подкоманда, то трактуем, как имя шахты и телепортим туда
		if(!$p->checkSubCommand($s, $subCmd, false)){
			$args[1] = $args[0];
			return $this->cmdTp($s, "mine", $args);
		}

		if(!$p->canUseSubCommand($s, $subCmd, $subCmdOrig)) return true;

		switch($subCmd){
			case "help":
				return $p->printHelp($s);

			case "list":
				return $this->cmdList($s);

			case "info":
				return $this->cmdInfo($s, $subCmd, $args);

			case "pos1":
				return $this->cmdPos($s, 1);

			case "pos2":
				return $this->cmdPos($s, 2);

			case "movepos":
				return $this->cmdMovePos($s, $subCmd, $args);

			case "create":
				return $this->cmdCreate($s, $subCmd, $args);//TODO проверить

			case "addrule":
				return $this->cmdAddRule($s, $subCmd, $args);

			case "delrule":
				return $this->cmdDelRule($s, $subCmd, $args);

			case "delete":
				return $this->cmdDelete($s, $subCmd, $args);

			case "fill":
				return $this->cmdFill($s, $subCmd, $args);

			case "fillall":
				$i = 0;
				foreach($p->mines as $mine){
					if($mine->isRulesSet()){
						$mine->fillMine();
						$i++;
					}
				}
				return $p->MSG($s, 1, "[✔] Обновлены {$i} шахт.");

			case "clear":
				return $this->cmdClear($s, $subCmd, $args);

			case "spawn":
				return $this->cmdSpawn($s, $subCmd, $args);

			case "tp":
				return $this->cmdTp($s, $subCmd, $args);

		}
		return false;
	}

	private function cmdList(CommandSender $s){
		$p = $this->plugin;
		$msg = "[✔] ======= Список шахт ======= ";
		foreach($p->mines as $mineNameLC => $mine){
			$msg .= $mine->getInfo();
		}
		return $p->MSG($s, 1, $msg);
	}

	private function cmdInfo(CommandSender $s, $subCmd, $args){
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		$p = $this->plugin;
		if(isset($args[1])){
			$mineNameLC = strtolower($args[1]);
			if(!isset($p->mines[$mineNameLC])) return $p->MSG($s, 0, "[✘] Нет шахты с именем " . $p->frn($args[1], 0) . ".");
			$mine = $p->mines[$mineNameLC];
			return $p->MSG($s, 1, "[✔] ======= Информация о шахте ======= " . $mine->getInfo());
		}else{
			if(!$s instanceof Player) return $p->MSG($s, 0, "[✘] Вы не указали имя шахты.");
			$pos = $s->getPosition();
			$mine = $p->getMineIfInside($pos);
			if($mine === null) return $p->MSG($s, 1, "[✔] Здесь нет шахт.");
			return $p->MSG($s, 1, "[✔] ======= Информация о шахте, в которой вы сейчас находитесь ======= " . $mine->getInfo());
		}
	}

	private function cmdMovePos(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		if(!isset($args[2])) return $p->MSG($s, 0, "[✘] Укажите номер вершины шахты §e1§a или §e2§a. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(!preg_match('/^(1|2)$/sim', $args[2])) return $p->MSG($s, 0, "[✘] Укажите правильно номер вершины шахты §e1§a или §e2§a. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		$posNum = $args[2];
		if(!isset($args[3])) return $p->MSG($s, 0, "[✘] Укажите координату смещения (§ex§a или §ey§a или §ez§a). Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(!preg_match('/^(x|y|z)$/sim', $args[3])) return $p->MSG($s, 0, "[✘] Укажите правильно координату смещения (должно быть §ex§a или §ey§a или §ez§a). Формат команды: " . $p->getCmdUsage($subCmd, 0));
		$axe = $args[3];
		if(!isset($args[4])) return $p->MSG($s, 0, "[✘] Укажите величину смещения. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(!preg_match('/^\-?\d+$/sim', $args[4]) || abs(intval($args[4]) > 250)) return $p->MSG($s, 0, "[✘] Укажите правильно величину смещения. Должно быть число от -250 до 250. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		$delta = intval($args[4]);

		$mine->movePos($posNum, $axe, $delta);
		return $p->MSG($s, 1, "[✔] Вершина §e{$posNum}§a шахты " . $p->frn($args[1], 1) . " смещена:" . $mine->getInfo(false));
	}

//TODO Комвнда tag name value
//TODO Глюк при посторной команде креате
	private function cmdCreate(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$s instanceof Player) return $p->MSG($s, 0, "[✘] Эта команда доступна только в игре.");
		if(!isset($args[1])) return $p->MSG($s, 0, "[✘] Вы не указали имя шахты. " . $p->getCmdUsage($subCmd, 0, true));
		$mineName = $args[1];
		$mineNameLC = strtolower($mineName);
		if(isset($p->mines[$mineNameLC])) return $p->MSG($s, 0, "[✘] Шахта с именем " . $p->frn($args[1], 0) . " уже есть.");
		$reservedList = $p->validateNameAgainstSubCommands($mineNameLC);
		if($reservedList !== true){
			return $p->MSG($s, 0, "[✘] Имя шахты не должно совпадать с подкомандой или ее алиасом. \n§eСписок запрещенных имен шахт: " . $p->ec($reservedList, 3, '§b') . ".");
		}

		$pNameLC = strtolower($s->getName());
		$ses = &$p->sessions;

		if(!isset($ses[$pNameLC])){
			$ses[$pNameLC] = [];
			$ses[$pNameLC][0] = $mineName;
			return $p->MSG($s, 1, "[✔] Тапните блок для установки перой точки шахты. Или задайте первую точку шахты командой " . $p->getCmdUsage('pos1', 1) . '.');
		}
		$sesP = $ses[$pNameLC];
		if(isset($sesP[1]) && isset($sesP[2])){
			$mine = $p->getMineIfIntersect($sesP[1], $sesP[2]);
			if($mine !== null){
				unset($p->sessions[$pNameLC]);
				return $p->MSG($s, 0, "[✘] Новая шахта пересекается с шахтой " . $p->frn($mine->getName(), 0) . ". Попробуйте снова создать шахту в другом месте.");
			}
			return $p->createMine($s, $mineName, $sesP[1], $sesP[2], $s->getLevel());
		}
		return $p->MSG($s, 1, "[✔] Тапните другой блок, чтобы создать шахту. Или задайте другую точку шахты командой " . $p->getCmdUsage('pos' . isset($sesP[1]) ? 2 : 1, 1) . '.');
	}


	private function cmdAddRule(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		if(!isset($args[2])) return $p->MSG($s, 0, "[✘] Укажите параметр §eid_блока§c. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(!preg_match('/(\d+)(?::(\d+))*/m', $args[2])) return $p->MSG($s, 0, "[✘] Параметр §eid_блока§c указан не верно. Должно быть число или два числа, разделенных двоеточием: id:damage. Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(!isset($args[3])) return $p->MSG($s, 0, "[✘] Укажите параметр §eдоля§c (доля заполнения шахты указанным блоком). Формат команды: " . $p->getCmdUsage($subCmd, 0));
		if(intval($args[3]) < 1) return $p->MSG($s, 0, "[✘] Необходимо правильно указать параметр §eдоля§c (доля заполнения шахты указанным блоком должна быть числом больше 0). Формат команды: " . $p->getCmdUsage($subCmd, 0));

		$fillPart = max(1, intval($args[3]));
		$minpc = isset($args[4]) ? max(0, min(90, intval($args[4]))) : null;
		if(!$mine->addRule($args[2], $fillPart, $minpc)) return $p->MSG($s, 0, "[✘] При добавлении правила заполнения шахты произошла неизвестная ошибка");
		$msg = "[✔] Правило заполнения шахты " . $p->frn($args[1], 1) . " установлено: " . $mine->getRulesInfo();
		$msg .= "\n§eДля заполнения шахты используйте " . $p->getCmdUsage('fill', 1);
		return $p->MSG($s, 1, $msg);
	}

	private function cmdDelRule(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		$mine->deleteRule($args[2]);
		$msg = "[✔] Правило заполнения шахты " . $p->frn($args[1], 1) . "  удалено. Текущие правила: ";
		$rulesInfo = $mine->getRulesInfo();
		if($rulesInfo == ''){
			$msg .= "не заданы.";
			$msg .= "\n§eДля добавления новго правила используйте " . $p->getCmdUsage('addrule', 1);
		}else{
			$msg .= $rulesInfo;
			$msg .= "\n§eДля заполнения шахты используйте " . $p->getCmdUsage('fill', 1);
		}
		return $p->MSG($s, 1, $msg);
	}

	private function cmdDelete(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		unset($p->mines[strtolower($args[1])]);
		$p->saveMinesConfig();
		return $p->MSG($s, 1, "[✔] Шахта " . $p->frn($args[1], 1) . " удалена.");
	}

	private function cmdFill(CommandSender $s, $subCmd, $args){
		//TODO проверить заполнение предметами с ID > 255 - если не работает, удалить нафиг отовсюду, прописать ограничения
		//Проверить, работает ли damage - если не работает, удалить нафиг отовсюду
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		if(!$mine->isRulesSet()) return $p->MSG($s, 0, "[✘] Сначала нужно установить правило заполнения шахты командой " . $p->getCmdUsage('addrule', 0));
		$mine->fillMine();
		return $p->MSG($s, 1, "[✔] Обновление шахты " . $p->frn($args[1], 1) . ".");
	}

	private function cmdClear(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine)) return true;
		/* @var Mine $mine */
		$mine->fillMine(true);
		return $p->MSG($s, 1, "[✔] Заполнение шахты " . $p->frn($args[1], 1) . "  воздухом.");
	}

	/**
	 * @param CommandSender $s
	 * @param               $pn
	 *
	 * @return bool
	 */
	private function cmdPos(CommandSender $s, $pn){
		$p = $this->plugin;
		if(!$s instanceof Player) return $p->MSG($s, 0, "[✘] Эта команда доступна только в игре.");
		$pNameLC = strtolower($s->getName());

		$pos = $s->getPosition();

		$mine = $p->getMineIfInside($pos);
		if($mine !== null){
			return $p->MSG($s, 0, "[✘] Это место уже находится в шахте " . $p->frn($mine->getName(), 0) . ". Выполните команду в другом месте.");
		}

		$OtherPN = $pn == 2 ? 1 : 2;
		$OtherPos = 'pos' . $OtherPN;

		$ses = &$p->sessions;
		if(!isset($ses[$pNameLC])){
			$ses[$pNameLC] = [];
		}
		$sesP = &$ses[$pNameLC];
		$sesP[$pn] = $pos;
		if(!isset($sesP[$OtherPN])){
			$msg = "[✔] Установлена " . $p->ec($pn . '-я', 1, '§b') . " точка шахты. " . $p->fCrd($pos, true) . '§a. ';
			$msg .= "Теперь задайте в нужном месте $OtherPN-ю точку командой " . $p->getCmdUsage($OtherPos, 1);
			return $p->MSG($s, 1, $msg);
		}
		if(!isset($sesP[0])){
			$msg = "[✔] Обе точки шахты заданы координатами " . $p->fCrd($sesP[1]) . "§a и " . $p->fCrd($sesP[2]);
			$msg .= ".§a Объем шахты: " . $p->ec($p->getCreatingMineVolume($pNameLC), 1, '§d') . " блоков. ";
			$msg .= "\n§aВыполните команду " . $p->getCmdUsage('create', 1) . " для создания шахты";
			return $p->MSG($s, 1, $msg);
		}
		return $p->createMine($s, $sesP[0], $sesP[$OtherPN], $pos, $s->getLevel());
	}

	/**
	 * @param $s
	 * @param $subCmd
	 * @param $args
	 *
	 * @return bool
	 */
	private function cmdSpawn($s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine, true)) return true;
		/* @var Mine $mine */
		/* @var Player $s */
		$pos = $s->getPosition();
		//Проверяем допустимое расстояние от шахты до ее спавна
		$p1 = $mine->getPos1();
		$p2 = $mine->getPos2();
		$DistX = min(abs($p1->getX() - $pos->getX()), abs($p2->getX() - $pos->getX()));
		$DistY = min(abs($p1->getY() - $pos->getY()), abs($p2->getY() - $pos->getY()));
		$DistZ = min(abs($p1->getZ() - $pos->getZ()), abs($p2->getZ() - $pos->getZ()));
		$distance = max($DistX, $DistY, $DistZ);
		if($distance > $p->maxSpawnDistance){
			return $p->MSG($s, 0, "[✘] Нельзя устанавливать спавн шахты дальне " . $p->maxSpawnDistance . " блоков (сейчас - $distance).");
		}
		$mine->spawnPos = new Vector3($pos->getX(), $pos->getY(), $pos->getZ());
		$p->saveMinesConfig();
		return $p->MSG($s, 1, "[✔] Спаун шахты " . $p->frn($args[1], 1) . " установлен по координатам " . $p->fCrd($mine->spawnPos));
	}

	private function cmdTp(CommandSender $s, $subCmd, $args){
		$p = $this->plugin;
		if(!$this->check1($s, $subCmd, $args, $mine, true)) return true;
		/* @var Mine $mine */
		/* @var Player $s */

		$level = $mine->getLevel();
		$spawn = $mine->getSpawnPos();

		//TODO проверить
		$pos = $level->getSafeSpawn($spawn);
		if($pos === false){
			$y = $p->enoughFreeSpace($spawn, $level) ? $spawn->getY() : $level->getHighestBlockAt($spawn->getX(), $spawn->getZ()) + 1;
			$pos = new Position($spawn->getX(), $y, $spawn->getZ(), $level);
		}

		if($s->teleport($pos)){
			return $p->MSG($s, 1, "[✔] Телепортация к шахте " . $p->frn($mine->getName(), 1));
		}
		return $p->MSG($s, 0, "[✘] Не удалось переместитиься в шахту " . $p->frn($mine->getName(), 1) . " по неизвестной причине.");
	}

	/**
	 * @param CommandSender $s
	 * @param string        $subCmd
	 * @param array         $args
	 * @param Mine          $mine
	 * @param bool          $chekIsPlayer
	 *
	 * @return bool
	 */
	private function check1(CommandSender $s, $subCmd, $args, &$mine, $chekIsPlayer = false){
		$p = $this->plugin;
		if($chekIsPlayer && !($s instanceof Player)) return $p->MSG($s, 0, "[✘] Эта команда доступна только в игре.");
		if(!isset($args[1])) return $p->MSG($s, 0, "[✘] Вы не указали имя шахты. " . $p->getCmdUsage($subCmd, 0, true), false);
		$mineNameLC = strtolower($args[1]);
		if(!isset($p->mines[$mineNameLC])) return $p->MSG($s, 0, "[✘] Нет шахты с именем " . $p->frn($args[1], 0) . ".", false);
		$mine = $p->mines[$mineNameLC];
		return true;
	}

}
