<?php
namespace PMC_AutoMine;

use PMC_AutoMine\lib\lib_PMC;
use PMC_AutoMine\Tasks\MineResetTask;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\command\PluginCommand;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\level\Level;
use pocketmine\level\Position;
use pocketmine\math\Vector3;
use pocketmine\utils\Config;

class PMC_AutoMine extends lib_PMC implements CommandExecutor, Listener {

	const ITEM_ID_RESTRICT = 0xff;
	const ITEM_DAMAGE_RESTRICT = 0x0f;

	public $sessions;
	/** @var  Config */
	public $mineData;
	/** @var  Mine[] */
	public $mines;
	/** @var  RegionBlocker */
	private $regionBlocker;

	/** @var  int $maxSpawnDistance максимально допустимое расстояние от края шахты до спавна */
	public $maxSpawnDistance;

	/** @var  int $minFillRatio минимальный % заполнения шахты, после которого она автообновляется */
	public $minFillRatio;

	/** @var  int $checkInterval интервал времени, в секундах, через которое производится проверка шахт на заполнение */
	public $checkInterval;

	public function onLoad(){
		@mkdir($this->getDataFolder());
		$this->saveDefaultConfig();
	}

	public function onEnable(){
		$cfg = $this->getConfig()->getAll();
		$this->maxSpawnDistance = $cfg['max-spawn-distance'];
		$this->minFillRatio = $cfg['min-fill-ratio'];
		$this->checkInterval = $cfg['check-interval'];

		$this->mineData = new Config($this->getDataFolder() . "mines.yml", Config::YAML, []);
		$this->mines = [];
		$this->parseMinesConfig();
		$this->getServer()->getPluginManager()->registerEvents($this, $this);
		$this->regionBlocker = new RegionBlocker($this);
		$this->sessions = [];
		$this->initCommandParams();

		/** @var PluginCommand $ftCmd */
		$ftCmd = $this->getCommand("mine");
		$ftCmd->setExecutor(new Commands\MineCommands($this));
		if(isset($cfg['check-interval']) && intval($cfg['check-interval']) > 0){
			$interval = intval($cfg['check-interval']);
			$this->getServer()->getScheduler()->scheduleDelayedRepeatingTask(new Tasks\RenewTask($this), 200, 20 * $interval);//
		}
		//ss убрать
		//$this->getServer()->getScheduler()->scheduleDelayedTask(new Tasks\TestReset($this), 100);
	}

//	public function onCommand(CommandSender $s, Command $cmd, $label, array $args){
//		echo("Вызван onCommand в PMC_AutoMine команда " . $cmd->getName() . " \n");
//	}

//TODO проверить
	public function onBlockTap(PlayerInteractEvent $event){
		$s = $event->getPlayer();
		$pNameLC = strtolower($s->getName());
		if(!isset($this->sessions[$pNameLC])) return true;
		$sesP = &$this->sessions[$pNameLC];

		if(!isset($sesP[1]) && !isset($sesP[2])){
			$sesP[1] = $event->getBlock();
			return $this->MSG($s, 1, "Тапните другой блок, чтобы создать шахту");
		}
		$pos = $event->getBlock();

		$mine = $this->getMineIfInside($pos);
		if($mine !== null){
			return $this->MSG($s, 0, "[✘] Эта точка уже находится в шахте " . $this->frn($mine->getName(), 0) . ". Тапните в другом месте.");
		}

		$sesP[isset($sesP[1]) ? 2 : 1] = $pos;
		if(!isset($sesP[0])) return $this->MSG($s, 1, "[✔] Выполните команду " . $this->getCmdUsage('create', 1) . " для создания шахты");

		return $this->createMine($s, $sesP[0], $sesP[1], $sesP[2], $pos->getLevel());
	}

	/**
	 * @param CommandSender $sender
	 * @param string        $mineName
	 * @param Vector3       $pos1
	 * @param Vector3       $pos2
	 * @param Level         $level
	 *
	 * @return bool
	 */
	public function createMine($sender, $mineName, $pos1, $pos2, $level){
		$mineNameLC = strtolower($mineName);
		$tags = ['author' => $sender->getName(), 'created' => date('Y.m.d H:i:s')];
		$this->mines[$mineNameLC] = new Mine($this, $mineName, $pos1, $pos2, $level, [], null, $tags);
		$this->MSG($sender, 1, "[✔] Шахта " . $this->frn($mineName, 1) . " создана! Теперь надо задать правиа заполнения шахты командой " . $this->getCmdUsage('addrule', 1) . '.');
		unset($this->sessions[strtolower($sender->getName())]);
		$this->saveMinesConfig();
		return true;
	}

	public function saveMinesConfig(){
		$this->mineData->setAll([]);
		foreach($this->mines as $maineNameLC => $mine){
			$this->mineData->set($mine->getName(),
				[
					'pos1'  => $this->packCoords($mine->getPos1()),
					'pos2'  => $this->packCoords($mine->getPos2()),
					'spawn' => $this->packCoords($mine->getSpawnPos()),
					'rules' => $mine->getRules(),
					'level' => $mine->getLevel()->getName(),
					'tags'  => $mine->tags
				]
			);
		}
		$this->mineData->save();
	}

	public function parseMinesConfig(){
		foreach($this->mineData->getAll() as $n => $m){
			if(!isset($m['pos1']) || !isset($m['pos2'])) continue;
			$level = $this->getServer()->getLevelByName($m['level']);
			if($level == null) continue;
			$rules = [];
			if(isset($m['rules']) && is_array($m['rules'])){
				foreach($m['rules'] as $itemId => $rule){
					if(!preg_match('/(\d+)(?::(\d+))*/m', $itemId, $matches)) continue;
					$itemId = (intval($matches[1]) & self::ITEM_ID_RESTRICT);
					if(isset($matches[2]) && intval($matches[2]) > 0){
						$itemId .= ':' . (intval($matches[2]) & self::ITEM_DAMAGE_RESTRICT);
					}
					if(!isset($rule['fill-part']) || intval($rule['fill-part']) < 1) continue;
					$rules[$itemId] = [
						'name'      => $this->getItemNameRU($itemId),
						'fill-part' => $rule['fill-part']
					];
					if(isset($rule['min-fill-ratio']) && intval($rule['min-fill-ratio']) > 0){
						$rules[$itemId]['min-fill-ratio'] = $rule['min-fill-ratio'];
					}
				}
			}
			$v1 = $this->compactCoords($m['pos1'], 2);
			$v2 = $this->compactCoords($m['pos2'], 2);
			$spawn = isset($m['spawn']) ? $this->compactCoords($m['spawn'], 2) : null;
			$tags = isset($m['tags']) ? $m['tags'] : [];
			$this->mines[strtolower($n)] = new Mine($this, $n, $v1, $v2, $level, $rules, $spawn, $tags);
		}
	}

	public function scheduleReset(MineResetTask $mineResetTask){
		$this->getServer()->getScheduler()->scheduleAsyncTask($mineResetTask);
	}

	/**
	 * @return RegionBlocker
	 */
	public function getRegionBlocker(){
		return $this->regionBlocker;
	}


	public function getCreatingMineVolume($pNameLC){
		$ses = $this->sessions;
		if(!isset($ses[$pNameLC]) || !isset($ses[$pNameLC][1]) || !isset($ses[$pNameLC][2])) return 0;
		/* @var Vector3|Position $pos1
		 * @var Vector3|Position $pos2
		 */
		$pos1 = $ses[$pNameLC][1];
		$pos2 = $ses[$pNameLC][2];
		return (abs($pos1->getFloorX() - $pos2->getFloorX()) + 1) * (abs($pos1->getFloorY() - $pos2->getFloorY()) + 1) * (abs($pos1->getFloorZ() - $pos2->getFloorZ()) + 1);
	}

	/**
	 * @param Vector3|Position $pos
	 *
	 * @return Mine|null
	 */
	public function getMineIfInside($pos){
		foreach($this->mines as $maineNameLC => $mine){
			if($mine->insideMine($pos)) return $mine;
		}
		return null;
	}

	/**
	 * @param Vector3|Position $p1
	 * @param Vector3|Position $p2
	 *
	 * @return Mine|null
	 */
	public function getMineIfIntersect($p1, $p2){
		foreach($this->mines as $maineNameLC => $mine){
			if($mine->intersectWithMine($p1, $p2)) return $mine;
		}
		return null;
	}

}
