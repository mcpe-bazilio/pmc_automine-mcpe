<?php
namespace PMC_AutoMine;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\level\Level;
use pocketmine\math\Vector3;

class RegionBlocker implements Listener {
	/** @var PMC_AutoMine $plugin */
	private $plugin;
	private $activeZones;

	public function __construct(PMC_AutoMine $PMC_AutoMine){
		$this->plugin = $PMC_AutoMine;
		$this->activeZones = [];
		$this->plugin->getServer()->getPluginManager()->registerEvents($this, $this->plugin);
	}

	public function onPlayerMove(PlayerMoveEvent $event){
		if(isset($this->activeZones[$event->getPlayer()->getLevel()->getId()])){
			foreach($this->activeZones[$event->getPlayer()->getLevel()->getId()] as $zone){
				if($this->isInsideZone($event->getTo(), $zone[0], $zone[1])){
					$event->setCancelled();
					$event->getPlayer()->sendMessage("§c[✘] Сейчас туда входить запрещено: обновляется шахта. Попробуйте зайти позже.");
					return;
				}
			}
		}
	}

	public function blockZone(Vector3 $p1, Vector3 $p2, Level $level){
		if(!isset($this->activeZones[$level->getId()])) $this->activeZones[$level->getId()] = [];
		$id = count($this->activeZones[$level->getId()]);
		$this->activeZones[$level->getId()][$id] = [$p1, $p2];
		$this->clearZone($level, $id);
		return $id;
	}

	public function freeZone($id, $level){
		if($level instanceof Level) $level = $level->getId();
		if(isset($this->activeZones[$level]) && isset($this->activeZones[$level][$id])){
			unset($this->activeZones[$level][$id]);
		}
	}

	protected function isInsideZone(Vector3 $test, Vector3 $p1, Vector3 $p2){
		return ($test->getX() >= $p1->getX() && $test->getX() <= $p2->getX() && $test->getY() >= $p1->getY() && $test->getY() <= $p2->getY() && $test->getZ() >= $p1->getZ() && $test->getZ() <= $p2->getZ());
	}

	protected function clearZone($level, $id){
		if($level instanceof Level) $level = $level->getId();
		if(isset($this->activeZones[$level]) && isset($this->activeZones[$level][$id])){
			foreach($this->plugin->getServer()->getOnlinePlayers() as $player){
				if($player->getLevel()->getId() === $level && $this->isInsideZone($player->getPosition(), $this->activeZones[$level][$id][0], $this->activeZones[$level][$id][1])){
					$player->teleport($player->getSpawn());
					$player->sendMessage("§e[!] Вы были телепортированы из шахты, которая сейчас будет обновляться.");
				}
			}
		}
	}
}
