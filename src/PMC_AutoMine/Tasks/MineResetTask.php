<?php
namespace PMC_AutoMine\Tasks;

use PMC_AutoMine\PMC_AutoMine;
use pocketmine\level\format\Chunk;
use pocketmine\level\Level;
use pocketmine\math\Vector3;
use pocketmine\scheduler\AsyncTask;
use pocketmine\Server;

class MineResetTask extends AsyncTask {
	const ITEM_ID_RESTRICT = PMC_AutoMine::ITEM_ID_RESTRICT;
	const ITEM_DAMAGE_RESTRICT = PMC_AutoMine::ITEM_DAMAGE_RESTRICT;

	private $chunks;
	private $pos1;
	private $pos2;
	private $fillRules;
	private $levelId;
	private $regionId;
	private $chunkClass;
	private $mineName;

	public function __construct(array $chunks, Vector3 $p1, Vector3 $p2, array $fillRules, $levelId, $regionId, $chunkClass, $mineName){
		$this->chunks = serialize($chunks);
		$this->pos1 = $p1;
		$this->pos2 = $p2;
		$this->fillRules = serialize($fillRules);
		$this->levelId = $levelId;
		$this->regionId = $regionId;
		$this->chunkClass = $chunkClass;
		$this->mineName = $mineName;
	}

	/**
	 * Actions to execute when run
	 *
	 * @return void
	 */
	public function onRun(){
		$chunkClass = $this->chunkClass;
		/** @var  Chunk[] $chunks */
		$chunks = unserialize($this->chunks);
		foreach($chunks as $hash => $binary){
			/** @noinspection PhpUndefinedMethodInspection */
			$chunks[$hash] = $chunkClass::fastDeserialize($binary);
		}
		$sum = [];

		$rules = unserialize($this->fillRules);
		$idMeta = [];
		$m = [];
		foreach($rules as $itemId => $rule){
			$idMeta[] = $itemId;
			$m[] = $rule['fill-part']; //TODO проверить
		}

		$sum[0] = $m[0];
		for($l = 1; $l < count($m); $l++) $sum[$l] = $sum[$l - 1] + $m[$l];
		$p1 = $this->pos1;
		$p2 = $this->pos2;
		for($x = $p1->getX(); $x <= $p2->getX(); $x++){
			for($y = $p1->getY(); $y <= $p2->getY(); $y++){
				for($z = $p1->getZ(); $z <= $p2->getZ(); $z++){
					$rnd = rand(0, end($sum));
					for($l = 0; $l < count($sum); $l++){
						if($rnd <= $sum[$l]){
							$hash = Level::chunkHash($x >> 4, $z >> 4);
							if(preg_match('/(\d+)(?::(\d+))*/m', $idMeta[$l], $matches)){
								$id = intval($matches[1]) & self::ITEM_ID_RESTRICT;
								$damage = isset($matches[2]) ? (intval($matches[2]) & self::ITEM_DAMAGE_RESTRICT) : null;
								if(isset($chunks[$hash])) {
									$chunks[$hash]->setBlock($x & 0x0f, $y & 0x7f, $z & 0x0f, $id, $damage);
								}
								$l = count($sum);
							}
						}
					}
				}
			}
		}
		$this->setResult($chunks);
	}

	public function onCompletion(Server $server){
		$chunks = $this->getResult();
		$plugin = $server->getPluginManager()->getPlugin("PMC_AutoMine");
		if($plugin instanceof PMC_AutoMine and $plugin->isEnabled()){
			$level = $server->getLevel($this->levelId);
			if($level != null){
				/** @var  Chunk[] $chunks */
				foreach($chunks as $hash => $chunk){
					Level::getXZ($hash, $x, $z);
					$level->setChunk($x, $z, $chunk);
				}
			}
			$plugin->getRegionBlocker()->freeZone($this->regionId, $this->levelId);
			$server->getLogger()->info("Авто-Шахта '" . $this->mineName . "' обновлена.");
		}
	}
}

