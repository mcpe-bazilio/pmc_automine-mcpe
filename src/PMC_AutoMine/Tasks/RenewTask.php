<?php

namespace PMC_AutoMine\Tasks;

use PMC_AutoMine\PMC_AutoMine;
use pocketmine\scheduler\PluginTask;


class RenewTask extends PluginTask {

	/** @var PMC_AutoMine $plugin */
	private $plugin;

	public function __construct(PMC_AutoMine $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
	}

	public function onRun($tick){
		$L = $this->plugin->getServer()->getLogger();
		$p = $this->plugin;
		$msg = '';
		$info = '';
		foreach($p->mines as $maineNameLC => $mine){
			if($mine->checkNeedReset($info)){
				$bRenew = true;
				$mine->fillMine();
				$msg .= "Шахта " . $mine->getName() . ' будет обновлена. ' . $info . "\n";
			}
		}
		if(count($p->mines) > 0){
			if($msg == ''){
				$L->info('Шахты ... не требуют обновления.');
			}else{
				$L->info(rtrim($msg,"\n"));
			}
		}
	}
}
